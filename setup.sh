#!/bin/sh
#debug=echo

for org_file in `ls vimrc vim.* tmux.conf gvimrc svndiff.sh gitdiff.sh *.bash`
do
	if [ -L ${HOME}/.${org_file} ]; then
		echo "[SKIP] ${org_file}"
	else
		${debug} ln -s ${PWD}/${org_file} ${HOME}/.${org_file}
		echo "[ OK ] crate symbolic link to ${HOME}/.${org_file}"
	fi
done

git --version > /dev/null
if [ $? -eq 0 ]; then
	if [ ! -d ${HOME}/.vim/neobundle.vim ]; then
		echo "  ... git cloning ..."
		git clone https://github.com/Shougo/neobundle.vim ${HOME}/.vim/neobundle.vim

		echo "  ... plugins installing by neobundle..."
		chmod +x ${HOME}/.vim/neobundle.vim/bin/neoinstall
		${HOME}/.vim/neobundle.vim/bin/neoinstall
		if [ $? -eq 0 ]; then
			echo "[OK] finished installing neobundle and some plugins."
		else
			echo "[NG] Not yet finishing neobundle."
		fi
	else
		echo "[SKIP] clone neobundle"
	fi
else
	echo "Not found git in your PC."
	echo "Please prepare the neobundle plugin by yourself."
fi
