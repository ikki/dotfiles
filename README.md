# How to use

## For Linux

At first, please clone this repository like the following tree structure.

	$ git clone git@bitbucket.org:ikki/dotfiles.git
	
````bash

    $(HOME)/
    ├── .vim
    └─── dotfiles.git/        <---- this repository
           ├── READmd
           ├── setup.bat
           ├── setup.sh
           ├── tmux_conf
           ├── vim.binary
           ├── vim.charcode
           ├── vim.keymap
           ├── vim.lightline
           ├── vim.neobundle
           ├── vim.unite
           :
           :
           └── vimrc
````

After cloning, you can run the setup.sh.

    $ sh setup.sh

# For Windows

Sorry, not yet.

