# 必要なもの
# fzf : https://github.com/junegunn/fzf
#       インタラクティブ・フィルタリングツール
#       - インストール方法 -
#       $ git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
#       $ ~/.fzf/install
# vim : diff 結果を確認、コミットログ記入

which vim > /dev/null
if [ $? ]; then
   export SVN_EDITOR=$(which vim)
fi

function svncommit() {
    # svn commit wrapper
    # ctrl-i or tab key でコミット対象のファイルを選択
    # diff を見る場合は ctrl-d で vim が立ち上がり diff をチェック
    # enter で vim が立ち上がり、コミットログを記入
    local commitlist key list
    while commitlist=$(svn status | grep "^[A-Z] " | fzf -m --expect=ctrl-d,enter | sed -e "s/^[A-Z] \+//g" -e "s/\n/ /g"); do
        # 先頭がキーの文字列
        key=$(echo $commitlist | awk '{print $1}')
        # 2番目以降がファイル名
        list=$(echo $commitlist | awk 'NF > 1 { for(i=2; i<NF; i++) {printf ("%s ", $i)} print $NF}')
        if [[ "$key" == 'ctrl-d' ]]; then
            LANG=C svn diff --diff-cmd=diff | perl -pe 's/^([-+]).*$/"\e[".($1eq"+"?32:31)."m$&\e["/e' | less -R -
            continue
        elif [[ "$key" == 'enter' ]]; then
            if [ "$list" != "" ]; then
                svn commit ${@:1} $list && break
            else
                echo "no need to commit." && break
            fi
        else
            break
        fi
    done
}

function svnsw() {
    # svn switch wrapper
    # right key でディレクトリ階層を進み、
    # left key でディレクトリ階層を遡る
    # enter key で svn switch を実行
    local repo_root out key dir
    repo_root=$(LANG=C svn info | grep "Repository Root:" | awk '{print $3}')
    while out=$(LANG=C svn list $repo_root | fzf --expect=right,left ); do
        mapfile -t out <<< "$out"
        key="${out[0]}"
        dir="${out[1]}"
        if [ -n "$dir" ]; then
            if [[ "$key" == 'right' ]];then
                repo_root=$repo_root/$dir
                continue
            elif [[ "$key" == 'left' ]];then
                repo_root=$(dirname $repo_root)
                continue
            else
                echo "Switching $repo_root/$dir ..."
                svn switch $repo_root/$dir && break
            fi
        fi
    done
}

function svnlog() {
    # svn log wrapper
    # svnlog <dir>
    # right key コミットログの詳細を確認
    # ctrl-d で diff の結果を vim で確認
    # ctrl-r で svn log の結果を再度取得
    local log_list arg_repo key body list rev diffst
    arg_repo=$1
    log_list=$(svn log $arg_repo | awk '/^r[0-9]+ +\|/ {rev=$1; user=$3; date=$5; time=$6} !(/^r[0-9]+ +\|/ || /^-+$/ || /^$/) { comment = comment $0 } /^-+$/ && rev { printf("%5s | %-20s | %s | %s | %s\n", substr(rev,0,5), substr(user,0,20), date, time, comment); comment=""}')
    while list=$(echo -e "$log_list" | fzf --expect=right,ctrl-d,ctrl-r); do
        mapfile -t list <<< "$list"
        key="${list[0]}"
        body="${list[1]}"
        rev=$(echo "$body" | awk '{print $1}')
        if [[ "$key" == 'right' ]]; then
            # Show more details.
            diffst=$(LANG=C svn diff --diff-cmd=diff $arg_repo ${rev/r/-c} | diffstat)
            svn log -$rev | echo -e "`cat`\n\n$diffst" | less -R -
            continue
        elif [[ "$key" == 'ctrl-d' ]]; then
            # Show diff
            # replace from r123 to -c123
            LANG=C svn diff --diff-cmd=diff $arg_repo ${rev/r/-c}| perl -pe 's/^([-+]).*$/"\e[".($1eq"+"?32:31)."m$&\e["/e' | less -R -
            continue
        elif [[ "$key" == 'ctrl-r' ]]; then
            # Refresh svn log
            log_list=$(svn log $arg_repo | awk '/^r[0-9]+ +\|/ {rev=$1; user=$3; date=$5; time=$6} !(/^r[0-9]+ +\|/ || /^-+$/ || /^$/) { comment = comment $0 } /^-+$/ && rev { printf("%5s | %-20s | %s | %s | %s\n", substr(rev,0,5), substr(user,0,20), date, time, comment); comment=""}')
            continue
        else
            break;
        fi
    done
}
