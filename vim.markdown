" Depends on plasticboy/vim-markdown
" https://github.com/plasticboy/vim-markdown

" Disable Folding
let g:vim_markdown_folding_disabled=1

