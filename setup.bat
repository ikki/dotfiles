ECHO off

ECHO Your home directory is %HOMEPATH%.

FOR %%F in (vimrc vim.*) DO ( 
	IF NOT EXIST "%HOMEPATH%\home\.%%F" (
		ECHO "LINK ==> %HOMEPATH%\home\_%%F"
		MKLINK "%HOMEPATH%\home\.%%F" "%CD%\%%F"
	) ELSE (
		ECHO "SKIP ==> %HOMEPATH%\home\.%%F"
	)
)

