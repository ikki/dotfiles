" VIM setting files.

"バックスペースを有効化
set backspace=indent,eol,start

" タブとスペースの可視化
"set list
"set listchars=

" タブの設定 (tab = 4space)
set tabstop=4
set autoindent
set expandtab
set shiftwidth=4

"対応した括弧を表示
set showmatch

" デフォルトで行数を表示
set nu

" マウス有効
"set mouse=a

" 256色表示にする
set t_Co=256

" tmux 背景色がおかしくなる問題対策
set t_ut=

" タイトルをウィンドウ枠に表示
set title

" 括弧入力時の対応する括弧を表示(表示時間は2)
set showmatch
set matchtime=2

" ディレクトリツリー表示カスタマイズ
let g:netrw_liststyle = 3
let g:netrw_list_hide = 'CVS,\(^\|\s\s\)\zs\.\S\+,\.svn'
" 'v'でファイルを開くときは右側に開く。(デフォルトが左側なので入れ替え)
let g:netrw_altv = 1
" 'o'でファイルを開くときは下側に開く。(デフォルトが上側なので入れ替え)
let g:netrw_alto = 1

" インクリメンタルサーチ有効
set incsearch

" 大文字小文字無視して検索
set ignorecase

set cursorline
hi CursorLine term=reverse cterm=none ctermbg=237

" ペーストモードF12キーで有効化(貼り付けの際にレイアウト崩れないようにする)
set pastetoggle=<F12>

filetype plugin on
filetype indent on

" ============  ag (The Silver Seacher ) の設定 ===============
let g:ag_prg="ag --column"

" * での検索や text-object 等での選択時に - で切らない
setlocal iskeyword& iskeyword+=-

" =============== 外部設定ファイルの読み込み ================
" キーマップの設定
source ~/.vim.keymap

" neobundle の設定
source ~/.vim.neobundle

" Unite の設定
source ~/.vim.unite

" lightline の設定
source ~/.vim.lightline

" バイナリ開く際の設定
source ~/.vim.binary

" 文字コード判定
source ~/.vim.charcode

" Markdown Highlight and Keymap
" source ~/.vim.markdown

" Neocomplete の設定
source ~/.vim.neocomplete

" CtrlP の設定
source ~/.vim.ctrlp

" =============== カラーテーマの設定 ================
"colorscheme wombat256mod
"colorscheme hybrid
colorscheme jellybeans
"colorscheme Tomorrow-Night-Bright

"ファイルタイプごとに単語色分け
syntax on

" clipboard を有効化
set clipboard=unnamed,unnamedplus,autoselect

" Folding
" set foldlevel=2

" riv.vim の設定
"let g:riv_auto_fold_auto_update=0
" バックアップを取らない
set nobackup

" シンタックスハイライト
"au BufNewFile,BufRead *.c call s:FTlpc()

" 編集中のバッファを移動する際に、保存しなくても良いようにする
set hidden

" ===== Ag (SilverSeacher) ====
if executable('ag')
    let g:ctrlp_use_caching=0
    let g:ctrlp_user_command='ag %s -i --hidden -g ""'
endif

" 2byte文字対応
set ambiwidth=double

" コマンドモードの補完
set wildmenu

" 保存するコマンド履歴の数
set history=5000

set notitle
