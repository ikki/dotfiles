if has('gui_macvim')
	colorscheme jellybeans
	set clipboard=unnamed
	set lines=90 columns=200
endif

set guifont=Cica\ 15
